from PIL import Image
import os

for root, dirs, files in os.walk('.'):
	for file in files:
		filename, fileext = os.path.splitext(file)
		if fileext == '.jpg' or fileext == '.tga':
			print(os.path.join(root, file))
			img = Image.open(os.path.join(root, file))
			if img.size == (8192, 4096):
				img = img.resize((4096, 2048), Image.NEAREST)
			elif img.size == (16384, 8192):
				img = img.resize((4096, 2048), Image.NEAREST)
			img.save(os.path.join(root, filename + '.png'))
